#include <inttypes.h>
#include <stdio.h>

#include "lib.h"

uint8_t blob[1024];

const aligned_at_4* get_one(void) {
    /* find the first address that is 4 byte aligned but *not* 8 byte aligned */
    int i = 0;
    while ((uint64_t)&blob[i] % 4 != 0 || (uint64_t)&blob[i] % 8 == 0) i++;
    aligned_at_4* ptr = (aligned_at_4*) &blob[i];
    ptr->data = 123;
    return ptr;
}