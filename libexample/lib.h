#include <inttypes.h>

#ifdef _MSC_VER
#define LIB_EXPORT __declspec(dllexport)
#define ALIGN_BEGIN(N) __declspec(align (N))
#define ALIGN_END(N)
#else
#define LIB_EXPORT
#define ALIGN_BEING(N)
#define ALIGN_END(N) __attribute__ (( aligned(N) ))
#endif

ALIGN_BEGIN(4) typedef struct {
    uint8_t data;
} ALIGN_END(4) aligned_at_4;

LIB_EXPORT const aligned_at_4* get_one(void);