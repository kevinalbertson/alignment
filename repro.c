/* 
 * A verification that setting BSON_ALIGN_OF_PTR to 8 increases alignment of bson_error_t.
 * 
 * Reproduce these results with the following.
 * 
 * Add VS2017 compiler, cl.exe, to your path with:
 * > C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Auxiliary\Build\vcvars32.bat
 * 
 * Then compile (with your include path):
 * > cl.exe repro.c /I "C:\Users\Q\bin\mongo-c-driver\include\libbson-1.0"
 * > .\repro.exe
 * align of bson_error_t is 4
 * 
 * Then, change the value of BSON_ALIGN_OF_PTR in bson-macros.h to 8 for 32 bit Windows and recompile.
 * > cl.exe repro.c /I "C:\Users\Q\bin\mongo-c-driver\include\libbson-1.0"
 * > .\repro.exe
 * align of bson_error_t is 8
 * 
 * To see the warnings from CDRIVER-2880, compile with /TP (to compile as C++):
 * > cl.exe /TP repro.c /I "C:\Users\Q\bin\mongo-c-driver\include\libbson-1.0"
 * C:\Users\Q\bin\mongo-c-driver\include\libbson-1.0\bson/bson-types.h(293): warning C4359: '_bson_value_t': Alignment specifier is less than actual alignment (8), and will be ignored.
 * C:\Users\Q\bin\mongo-c-driver\include\libbson-1.0\bson/bson-types.h(361): warning C4359: '<anonymous-tag>': Alignment specifier is less than actual alignment (8), and will be ignored.
 */

/* Uncomment the pragmas to remove the warning.
 * #pragma warning( push )
 * #pragma warning( disable : 4359)
 */
#include <bson/bson.h>
/* #pragma warning( pop )  */

#include <stdio.h>

int main() {
    printf("align of bson_error_t is %d\n", (int)__alignof(bson_error_t));
}