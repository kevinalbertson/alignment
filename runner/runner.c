#include <stdio.h>
#include <inttypes.h>

#ifdef _MSC_VER
#define ALIGNED(N) __declspec(align (N))
#else
#define ALIGNED(N) __attribute__ (( aligned(N) ))
#endif

#ifdef TEST_WITHOUT_MISALIGNMENT
/* Normally you'd want this. */
#include "lib.h"
#else
/* Instead if we lie and say aligned_at_4 has 8 byte alignment, we trigger UB. */
typedef struct {
    uint8_t data;
} ALIGNED(8) aligned_at_4;
#endif

const aligned_at_4* get_one(void);

int main(int argc, char** argv) {
    /* access something we get from the library */
    const aligned_at_4* x = get_one();
    printf("data=%" PRId8 "\n", x->data);
}