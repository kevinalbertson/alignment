/* A simple example of how to reproduce an alignment error that UBSAN would catch.
 * Compile with -fsanitize=undefined.
 */
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <inttypes.h>

#define aligned_at(x) typedef struct { uint8_t data; } __attribute__ (( aligned (x) )) aligned_at_##x
typedef struct { uint8_t data; } unaligned;

aligned_at(4);
aligned_at(8);
aligned_at(16);
aligned_at(32);

void print_divisibility(uint64_t addr) {
    printf(" %s divisible by 4\n", (addr % 4 == 0) ? "" : "not");
    printf(" %s divisible by 8\n", (addr % 8 == 0) ? "" : "not");
    printf(" %s divisible by 16\n", (addr % 16 == 0) ? "" : "not");
    printf(" %s divisible by 32\n", (addr % 32 == 0) ? "" : "not");
}

#define inspect(s) do { \
    s var; \
    uint64_t addr = (uint64_t) &var; \
    printf("struct of type %s, is at address: %" PRIu64 "\n", #s, addr); \
    print_divisibility(addr); \
} while(0)

int main() {
    inspect(unaligned);
    inspect(aligned_at_4);
    inspect(aligned_at_8);
    inspect(aligned_at_16);
    inspect(aligned_at_32);

    uint8_t* memory = (uint8_t*)malloc(1024);
    for (int i = 0; i < 1024; i++) memory[i] = (char) i;
    /* now, find an address that is divisible by four but not divisible by 8. */
    while((uint64_t)memory % 8 == 0 || (uint64_t)memory % 4 != 0) memory++;
    /* interpret that as an aligned struct. That should be misaligned access. */
    printf("memory\n");
    print_divisibility((uint64_t)memory);
    aligned_at_8* test = (aligned_at_8*)memory;
    printf("dereferencing a misaligned pointer is: %" PRId8 "\n", test->data);
}
