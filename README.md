This is all to test out what happens for the problem described in https://jira.mongodb.org/browse/CDRIVER-2880

## What I've figured out
- If libexample is built with a 4-byte aligned struct, then linked to an application which thinks the struct is 8-byte aligned, this results in misaligned access (according to ubsan).
- extern "C" doesn't solve this, we already do that in the C driver headers.
- Building libmongoc in 32-bit Windows will use 4-byte alignment (according to `__align`) for bson_t. I think ahat means that linking against libmongoc with a C++ in VS 2017 currently already does misaligned access.
- We cannot change the alignment under ENABLE_EXTRA_ALIGNMENT=OFF. We should not break ABI for the same configuration.

## Pending questions
- Is it really the case in CDRIVER-2880 that building libmongoc with VS C compiler uses 4-byte alignment, but including in a C++ project interprets as 8-byte?
- Does misaligned access *really* matter?
- What performance benefit does alignment really give us?
- Since we can't introduce an ABI break with ENABLE_EXTRA_ALIGNMENT, should we introduce a DISABLE_ALIGNMENT option?
- Does MSVC have a way to detect misalignment usage?
- Why doesn't compiling the C driver reproduce the warning C4359? Is there really a difference in Visual Studio C vs C++ compiler?
- Can we fix just CDRIVER-2880?

## Does misaligned access on Windows *really* matter? ##
It might. Several references say so:
- On Itanium architecture an exception is thrown: https://docs.microsoft.com/en-us/windows/desktop/winprog64/fault-alignments
- Probably not on x86: https://stackoverflow.com/questions/548164/mis-aligned-pointers-on-x86
- https://docs.microsoft.com/en-us/windows/desktop/midl/c-compiler-packing-issues:
```
Natural alignment is important because accessing misaligned data may cause an exception on some systems. Data can be marked for a safe manipulation when misaligned, but typically that involves a speed penalty that may be substantial on some platforms.
```
- Can I get ubsan


## Notes
Use windows.sh to set some useful cygwin variables.

Configure to build with 32-bit (omit the Win64 part of the generator), build example-client, then run:
```
. ./windows.sh
cd path/to/c/driver
"$CMAKE" -G "Visual Studio 15 2017"  -DENABLE_EXTRA_ALIGNMENT=OFF -DENABLE_SASL=OFF ../
"$MSBUILD" /M ./src/libmongoc/example-client.vcxproj > build-logs.txt
./src/libmongoc/Debug/example-client.exe
```

Windows cl options:
https://docs.microsoft.com/en-us/cpp/build/reference/compiler-options-listed-by-category?view=vs-2017

I think cl can invoke either the C or C++ compiler.

`/Tc` specifies a C source file.

`/Zp` specifies default packing size.
(This page)[https://docs.microsoft.com/en-us/cpp/c-language/storage-and-alignment-of-structures?view=vs-2017] indicates that in C it is 4, but C++ it is 8.

Reference:
http://www.catb.org/esr/structure-packing/

#pragma pack(show)
Shows 16 for 64 bit compiler, 8 for 32 bit... I would think it's 8 regardless if /Zp is 8 by default.

https://docs.microsoft.com/en-us/cpp/cpp/align-cpp?view=vs-2017
This explains the rules for alignment in structs:

```
The compiler uses these rules for structure alignment:

Unless overridden with __declspec(align(#)), the alignment of a scalar structure member is the minimum of its size and the current packing.

Unless overridden with __declspec(align(#)), the alignment of a structure is the maximum of the individual alignments of its member(s).

A structure member is placed at an offset from the start of its parent structure which is the smallest multiple of its alignment greater than or equal to the offset of the end of the previous member.

The size of a structure is the smallest multiple of its alignment greater than or equal to the offset of the end of its last member.

__declspec(align(#)) can only increase alignment restrictions.
```

