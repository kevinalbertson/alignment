/* Does 32 bit windows compiler differ the setting of /Zp in C and C++ compilers?
 * Answer: No, /Zp seems to be the same regardless o f C/C++. For some reason, it is only warned in
 * C++ compiler.
 * https://docs.microsoft.com/en-us/cpp/c-language/storage-and-alignment-of-structures?view=vs-2017
 * 
 * Compile in the command prompt by first setting the path to cl.exe, something like:
 * C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Auxiliary\Build\vcvars32.bat
 * Then running:
 * cl.exe zp.c
 */

#include <stdio.h>
#include <stdint.h>
#include <stddef.h>

/* References:
 * MS1: https://docs.microsoft.com/en-us/cpp/cpp/align-cpp?view=vs-2017
 */

/* [MS1] "Unless overridden with __declspec(align(#)), the alignment of a structure is the maximum of the individual alignments of its member(s)."
 * So this is aligned with max alignment of all members. */
struct naturally_aligned {
    uint8_t a;
    /* [MS1] "Unless overridden with __declspec(align(#)), the alignment of a scalar structure member is the minimum of its size and the current packing." */
    uint64_t b; /* So this is aligned with min { 8, Zp }. By default Zp is 8. */
};

/* But for this struct, we try to explicitly align at four, *less* than the default alignment if Zp is 8. */
/* [MS1] "__declspec(align(#)) can only increase alignment restrictions." So the specifier will be ignored. We can't decrease alignment. */
__declspec(align(4)) struct aligned_at_four {
    uint8_t a;
    uint64_t b;  /* gives a natural alignment of eight. */
};

__declspec(align(8)) struct struct_w_pointer {
    char* ptr;
};

int main() {
    printf("alignof aligned_at_four is %d\n", (int)__alignof(struct aligned_at_four));
    printf("alignof naturally_aligned is %d\n", (int)__alignof(struct naturally_aligned));
    printf("alignof struct_w_pointer is %d\n", (int)__alignof(struct struct_w_pointer));
    printf("sizeof pointer = %d\n", (int)(sizeof(void*)));

    /* TODO:
     * 1. Onclude the libbson header and prove that changing BSON_ALIGN_OF_PTR to only 8 changes the alignment of bson_error_t in 32-bit windows. Thus proving we cannot do this.
     * 2. Use #pragma warning in the client application to ignore the warning.
     * Because it doesn't have any member with > 4 byte alignment, the struct has 4 byte alignment with the specifier.
     */
}