all: librunner
	
libtest.so: lib.c lib.h
	clang -fsanitize=undefined -c lib.c
	clang -fsanitize=undefined -shared -o libtest.so lib.o

librunner: lib-runner.c libtest.so
	clang -o librunner -fsanitize=undefined lib-runner.c -L$(shell pwd) -ltest

clean:
	rm libtest.so lib.o
